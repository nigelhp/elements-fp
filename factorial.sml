
(* using a conditional *)
fun factorial n =
    if n = 0 then 1 else n * factorial(n - 1)

                                      
(* using pattern matching *)
fun fact 0 = 1
  | fact n = n * fact(n - 1)                     


(* protecting against a negative argument *)
exception NegativeArgumentException
              
local
    fun nocheck_fact 0 = 1
      | nocheck_fact n = n * nocheck_fact(n - 1)
in
fun checked_factorial n =
    if n < 0 then
        raise NegativeArgumentException
    else
        nocheck_fact n
end
