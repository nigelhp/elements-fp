(* We assume all expressions are integer expressions *)
(* We interpret 0 and 1 as false and true respectively *)

(* ========= *)
(* utilities *)
(* ========= *)
(* the const function *)
fun K x y = x
fun C f x y = f y x

fun foldleft f =
    let fun foldf a [] = a
          | foldf a (b :: x) = foldf (f a b) x
    in
        foldf
    end

fun member x a =
    List.exists (fn a' => a' = a) x
        
(* =============== *)
(* abstract syntax *)
(* =============== *)

datatype variable = Var of string

(* Note that if a variable occurs in an expression, it is its contents *)
(* (relative to some store) that is used in calculating the expression value. *)
(* We use the constructor Contents to turn a variable into an expression. *)                               
datatype expression = Constant of int
                    | Contents of variable
                    | Minus of (expression * expression)
                    | Greater of (expression * expression)
                    | Times of (expression * expression)

(* A command is one of: *)
(* - Assignment (in which a variable is assigned the value of an expression) *)
(* - Sequence (comprising a command to do first and a command to do second) *)
(* - Conditional (a test expression with commands for the true and false branches) *)
(* - While (a loop test expression and a loop body command) *)
datatype command = Assign of (variable * expression)
                 | Sequence of (command * command)
                 | Conditional of (expression * command * command)
                 | While of (expression * command)
                                
                                
(* =========== *)
(* interpreter *)
(* =========== *)
                                
datatype intvalue = Intval of int

(* store *)
datatype store = Store of (variable -> intvalue)

(* the store where all variables are initialized with Intval 0 for their contents *)                              
(* note equivalence with the constant function K here: fun K x y = x *)
val store0 = let fun f v = Intval 0
             in Store f end
                 
fun fetch (v, Store f) =
    f v

fun assoc [] oldassocs a =
    oldassocs a
  | assoc ((a1, b1) :: rest) oldassocs a =
    if a = a1 then b1
    else assoc rest oldassocs a
               
fun update (Store f, v, a) =
    Store (assoc [(v, a)] f)
          

(* evalexp: expression -> store -> intvalue *)
fun evalexp (Constant n) s =
    Intval n
  | evalexp (Contents v) s =
    fetch (v, s)
  | evalexp (Minus (e1, e2)) s =
    let val Intval n1 = evalexp e1 s
        and Intval n2 = evalexp e2 s
    in Intval (n1 - n2) end
  | evalexp (Times (e1, e2)) s =
    let val Intval n1 = evalexp e1 s
        and Intval n2 = evalexp e2 s
    in Intval (n1 * n2) end
  | evalexp (Greater (e1, e2)) s =
    let val Intval n1 = evalexp e1 s
        and Intval n2 = evalexp e2 s
    in
        if n1 > n2 then Intval (1)
        else Intval (0)
    end
        
exception NonBooleanValueTest
              
(* Note that store does not take commands directly, but instead takes partially *)
(* applied commands i.e. two functions from stores to stores. *)
(* switch : intvalue -> (store -> store) -> (store -> store) -> store -> store *)
fun switch (Intval 1) f g s = f s
  | switch (Intval 0) f g s = g s
  | switch (Intval n) f g s = raise NonBooleanValueTest
    
fun interpret (Assign (v, e)) s =
    let val a = evalexp e s
    in update (s, v, a) end
  | interpret (Sequence (c1, c2)) s =
    interpret c2 (interpret c1 s)
  | interpret (Conditional (e, c1, c2)) s =
    (* switch (evalexp e s) c1 c2 s *)
    (* Note that the published code (above) does not compile - the commands cannot *)
    (* be passed directly, but must first be partially interpreted to create *)
    (* functions of type (store -> store) *)
    switch (evalexp e s)
           (interpret c1)
           (interpret c2)
           s
  | interpret (While (e, c)) s =
    switch (evalexp e s)
           (interpret (While (e, c)) o interpret c)
           (fn x => x)
           s;


(* ================ *)
(* lexical analysis *)
(* ================ *)

datatype token = Ident of string
               | Symbol of string
               | Number of int

val keyword = member ["IF", "THEN", "ELSE", "ENDIF", "WHILE", "DO", "END"]

local
    fun lowercase s =
        s >= "a" andalso s <= "z"
    fun uppercase s =
        s >= "A" andalso s <= "Z"
in
fun letter s =
    lowercase s orelse uppercase s
end   

fun digit s =
    s >= "0" andalso s <= "9"
                        
fun keycheck s =
    if keyword s then Symbol s
    else Ident s

fun letdigetc a =
    if letter a then true else
    if digit a then true else
    member ["'", "_"] a

val layout = member [" ", "\n", "\t"]
val symbolchar = member ["*", "+", "/", "-", ">", ":", "=", ";"]

fun intofdigit s =
    ord (String.sub (s, 0)) - ord #"0"

exception LexicalError of string
                              
fun lexanal [] = []
  | lexanal (a :: x) = if layout a then lexanal x else
                       if a = "(" then Symbol "(" :: lexanal x else
                       if a = ")" then Symbol ")" :: lexanal x else
                       if letter a then getword [a] x else
                       if digit a then getnum (intofdigit a) x else
                       if symbolchar a then getsymbol [a] x else
                       raise LexicalError("unrecognised token " ^ String.concat (a :: x))
and getword l [] = [keycheck (String.concat (List.rev l))]
  | getword l (a :: x) = if letdigetc a
                         then getword (a :: l) x
                         else keycheck (String.concat (List.rev l)) :: lexanal (a :: x)
and getsymbol l [] = [Symbol (String.concat (List.rev l))]
  | getsymbol l (a :: x) = if symbolchar a
                           then getsymbol (a :: l) x
                           else Symbol (String.concat (List.rev l)) :: lexanal (a :: x)
and getnum n [] = [Number n]
  | getnum n (a :: x) = if digit a
                        then getnum (n * 10 + intofdigit a) x
                        else Number n :: lexanal (a :: x)
                                                 
(* ======= *)
(* parsers *)
(* ======= *)
datatype 'a possible = Ok of 'a
                     | Fail

type 'a parser = token list -> ('a * token list) possible      

(* parser operators *)
infixr 4 <&>
infixr 3 <|>
infix  0 modify

(* modify is essentially 'map' *)
fun (parser modify f) s =
    let fun modresult Fail        = Fail
          | modresult (Ok (x, s)) = Ok (f x, s)
    in
        modresult (parser s)
    end
        
(* to be read as "followed by" *)      
fun (parser1 <&> parser2) s =
    let fun pair a b = (a, b)
        fun parser2_after Fail          = Fail
          | parser2_after (Ok (x1, s1)) = (parser2 modify (pair x1)) s1
    in
        parser2_after (parser1 s)
    end
        
(* use the component parsers as alternatives *)      
fun (parser1 <|> parser2) s =
    let fun parser2_if_fail Fail = parser2 s
          | parser2_if_fail x    = x
    in
        parser2_if_fail (parser1 s)
    end
        
(* always succeeds and does not consume any tokens *)      
fun emptyseq s = 
    Ok ([], s)
       
(* note list is used to represent optionality here *)     
fun optional pr =
    let fun consonto x a = a :: x
    in
        (pr modify (consonto [])) <|> emptyseq
    end
      
(* repeatedly uses pr until it fails; collecting a list of the items found *)                            
fun sequence pr =
    let fun seqpr s = ((pr <&> seqpr modify (op ::)) <|> emptyseq) s
    in 
        seqpr 
    end
        
(* looks for a font, followed by a list of prs spearated by sep, and ending *)
(* with a back.  Only the items returned by pr are kept in the result. *)
(* Note that this implementation depends on link - which generalizes @ to *)
(* allow several lists to be appened (think flatten) *)      
fun seqwith (front, sep, back) pr =
    let val accumulate = foldleft
        fun cons a x = a :: x                       
        val revonto = accumulate (C cons)
        val rev = revonto []
        fun link list = rev (accumulate revonto [] list)
        fun fst (x, y) = x
        fun snd (x, y) = y
        val sep_pr = sep <&> pr modify snd
        val items = pr <&> sequence sep_pr modify (op ::)
    in
        front <&> optional items <&> back modify (link o fst o snd) 
    end

(* generalizes <&> for use with more than 2 parsers (of the same type) *)      
fun parserList [] = emptyseq
  | parserList (pr :: rest) = pr <&> (parserList rest) modify (op ::)
                                 
(* generalizes <|> for use with a list of parsers to be tried in turn as alternatives *)                                 
fun alternatives [] = K Fail
  | alternatives (pr :: rest) = pr <|> alternatives rest
                                   
(* basic parsers *)
                                   
fun number (Number n :: s) = Ok (n, s)
  | number other           = Fail
                                 
fun variable (Ident x :: s) = Ok (Var x, s)
  | variable other          = Fail
                                  
fun literal a (Symbol x :: s) = if a = x then Ok (x, s) else Fail
  | literal a other           = Fail
                                    
(* expression parsers *)

exception IllegalStateException
              
fun exp s =
    (aexp <&> optional (literal ">" <&> aexp) modify opt_compare) s
and aexp s =
    (bexp <&> optional (literal "-" <&> exp) modify opt_sub) s
and bexp s =
    (cexp <&> optional (literal "*" <&> cexp) modify opt_mul) s
and cexp s =
    ((literal "(" <&> exp <&> literal ")" modify unparenth)
         <|> (number modify Constant)
         <|> (variable modify Contents)) s
                                         
(* auxiliary ops *)
                                         
and unparenth (bra, (e, ket)) =
    e
and opt_compare (e1, []) = e1
  | opt_compare (e1, [(oper, e2)]) = Greater (e1, e2)
  | opt_compare other = raise IllegalStateException
and opt_sub (e1, []) = e1
  | opt_sub (e1, [(oper, e2)]) = Minus (e1, e2)
  | opt_sub other = raise IllegalStateException
and opt_mul (e1, []) = e1
  | opt_mul (e1, [(oper, e2)]) = Times (e1, e2) 
  | opt_mul other = raise IllegalStateException;

(* command parsers *)

fun command s = (unitcom <&> optional (literal ";" <&> command) modify opt_seq) s
and unitcom s =
    (whilecom <|> ifcom <|> assign) s
and whilecom s =
    (literal "WHILE" <&> exp <&>
             literal "DO" <&> command <&>
             literal "END" modify mk_while_node) s
and ifcom s =
    (literal "IF" <&> exp <&>
             literal "THEN" <&> command <&>
             literal "ELSE" <&> command <&>
             literal "ENDIF" modify mk_if_node) s
and assign s =
    (variable <&> literal ":=" <&> exp modify mk_assign_node) s
                                                              
(* auxiliary ops *)   
                                                              
and opt_seq (c1, []) = c1
  | opt_seq (c1, [(semicol, c2)]) = Sequence (c1, c2) 
  | opt_seq other = raise IllegalStateException
and mk_while_node (w, (ex, (d, (com, e)))) =
    While (ex, com)
and mk_if_node (i, (ex, (t, (c1, (e, (c2, f)))))) =
    Conditional (ex, c1, c2)
and mk_assign_node (v, (coleq, e)) =
    Assign (v, e);

(* the main parser *)

fun lit (Symbol s) = s
  | lit (Number n) = Int.toString n
  | lit (Ident s) = s

exception ParseError
exception SyntaxError of string;

local fun stringwith (front, sep, back) list =
          front ^ (String.concatWith sep list) ^ back
in
fun report Fail = raise ParseError
  | report (Ok (c, [])) = c
  | report (Ok (c, x)) = raise SyntaxError(stringwith ("Syntax Error\nUnparsed:-\n", " ", "\n") (map lit x))
end
    
val main_parser = report o command;

(* ======== *)
(* examples *)
(* ======== *)

(* abstract command interpretation *)
(* X := 1 - Y; X := X * X *)
let val c = Sequence (Assign (Var "X", Minus (Constant 1,
                                              Contents (Var "Y"))),
                      Assign (Var "X", Times (Contents (Var "X"),
                                              Contents(Var "X"))))
    val (Store f) = interpret c store0
in f (Var "X") end;

(* main example *)    
Control.Print.printLength := 100;
val input = "WHILE X > Y DO X:= X - 1; Z := Z * Z END";

(* convert to a string list representation where a string represents a single char *)
(* note that the book was written prior to the introduction of the char type *)
val to_lex = List.map Char.toString (explode input);
val tokens = lexanal to_lex;

(* create a store containing initial values for X Y Z *)
(* as we start with store0 all values have a default of 0 if unspecified *)
val storex = update (store0, Var "X", Intval 10);
val storey = update (storex, Var "Y", Intval 6);
val storez = update (storey, Var "Z", Intval 2);

                     
let val cmd = main_parser tokens
    val (Store f) = interpret cmd storez
in f (Var "Z") end;

(* result should be 65536 *)
