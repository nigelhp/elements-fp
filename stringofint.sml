(* limit scope of non-robust auxiliary functions *)

local
    fun digittochar n =
        chr(n + ord #"0")
    fun digittostr n =
        str(digittochar n)
    fun stringofnat n =
        if n < 10 then
            digittostr n
        else
            stringofnat(n div 10) ^ digittostr(n mod 10)
in
fun stringofint n =
    if n < 0 then
        "~" ^ stringofnat(~n)
    else
        stringofnat(n)
end
