(* fold-right over non-empty list *)
fun foldr1 f [x] = x
  | foldr1 f (x :: y :: xs) =
    f x (foldr1 f (y :: xs))                                

fun concat xs =
    List.foldr List.@ [] xs      

fun scan' (f, z, []) = [] 
  | scan' (f, z, x :: xs) = scan (f, f(z, x), xs)  
and scan (f, z, xs) =
    z :: scan'(f, z, xs);

fun takewhile p [] = [] 
  | takewhile p (h :: t) =
    if p h then h :: (takewhile p t) else []

                                              
(* from Bird / Wadler, "Introduction to Functional Programming" - Section 4.3 *)
type text = char list

(* interop between text and SML's standard string *)
fun textFromString s =
    explode s

fun textToString t =
    implode t

            
type line = char list
type word = char list
type para = line list

local
    fun insert a xs ys =
        xs @ (a :: ys)
in
fun unlines lines =
    foldr1 (insert #"\n") lines

fun unwords words =
    foldr1 (insert #" ") words

fun unparas paras =
    foldr1 (insert []) paras
end


local
    fun breakon a x xss =
        if a = x then [] :: xss
        else (x :: (List.hd xss)) :: (List.tl xss)
    fun uncurry f (a, b) =
        f a b
in
fun lines text =
    List.foldr (uncurry (breakon #"\n")) [[]] text

fun words line =
    List.filter (fn (l) => not (null l))
                (List.foldr (uncurry (breakon #" ")) [[]] line)

fun paras lines =
    List.filter (fn (p) => not (null p))
                (List.foldr (uncurry (breakon [])) [[]] lines)
end

fun countlines text =
    length (lines text)

fun countwords text =
    length (concat (map words (lines text)))

fun countparas text =
    length (paras (lines text))

fun parse text =
    map (map words) (paras (lines text))

fun unparse wpl =
    unlines (unparas (map (map unwords) wpl))

fun normalise text =
    (unparse o parse) text


fun linewords para =
    concat (map words para)

fun textparas text =
    map linewords (paras (lines text))

fun greedy m ws =
    let val foo = scan((fn (n, word) => n + length(word) + 1), ~1, ws)
        val bar = takewhile (fn x => x <= m) foo
    in
        length(bar) - 1
    end
        
fun fill m [] = [] 
  | fill m ws =
    let val n = greedy m ws
        val firstln = List.take(ws, n)
        val restwds = List.drop(ws, n)
    in
        [firstln] @ fill m restwds
    end

fun filltext m text =
    unparse (map (fill m) (textparas text))
