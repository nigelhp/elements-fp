(* ******************* *)
(* auxiliary functions *)
(* ******************* *)

exception NegativeArgumentException
              
(* returns 0 for any list not containing positive integers (including []); *)
(* else the max integer. *)
fun maxposlist intlist =
    foldl Int.max 0 intlist

local
    fun stringcopy0(0, s) = ""
      | stringcopy0(n, s) = stringcopy0(n - 1, s) ^ s
    fun stringcopy(n, s) =
        if n < 0 then
            raise NegativeArgumentException
        else
            stringcopy0(n, s)
in
(* returns a string consisting of n spaces *)
fun spaces n =
    stringcopy(n, " ")

(* returns a string consisting of n dashes *)
fun dashes n =
    stringcopy(n, "-")
end

(* returns a list comprised of n copies of a *)
local
    fun copy0 0 a = [] 
      | copy0 n a = a :: copy0 (n - 1) a 
in
fun copy n a =
    if n < 0 then
        raise NegativeArgumentException
    else
        copy0 n a
end

(* returns the elements of list separated by sep, *)
(* with a leading front and trailing back *)
fun stringwith (front, sep, back) list =
    let fun sepback [] = back 
          | sepback [h] = h ^ back
          | sepback (h :: t) = h ^ sep ^ sepback t
    in
        front ^ sepback list
    end

(* returns the increasing list of integers in the range m..n; else [] if n < m *)
infix 4 upto
fun m upto n =
    if m <= n
    then m :: ((m + 1) upto n)
    else []

(* returns the list formed by linking together the lists in llist (a list of *)
(* lists) but with a copy of the list sep between each one and with front and *)
(* back adjoined to the ends of the final list. *)
fun linkwith (front, sep, back) llist =
    let fun sepback [] = back
          | sepback [h] = h @ back
          | sepback (h :: t) = h @ sep @ sepback t
    in
        front @ sepback llist
    end
        
(* a curried version of the string concatenation operation *)
fun concat s1 s2 =
    s1 ^ s2

(* convert a list of rows into a list of columns *)
fun transpose [] = []
  | transpose listlist =
    if List.exists null listlist
    then []
    else (map hd listlist) :: transpose(map tl listlist)

(* joins two lists element-by-element using the binary function (similar to *)
(* zip).  If one list is shorter than the other, the remainder of the longer *)
(* list is kept as-is. *)
fun splice f [] y = y
  | splice f x [] = x
  | splice f (a :: x) (b :: y) =
    f a b :: splice f x y

exception SpliceAtException of string
        
(* counts n elements along the first list before performing a splice. *)
(* assumes n >= 0 *)
fun spliceat 0 f x y = splice f x y
  | spliceat n f (a :: x) y = a :: spliceat (n - 1) f x y
  | spliceat n f [] y = raise SpliceAtException "spliceat n f []"

(* returns the m elements from x that follow the first n elements *)
fun sublist n m x =
    List.take (List.drop(x, n), m)
    
        
(* *********************** *)
(* picture representation  *)
(* *********************** *)

(* depth, width, lines *)
type picture = (int * int * string list) 

val nullpic = (0, 0, [])


(* ****************** *)
(* picture operations *)
(* ****************** *)

(* returns a picture comprising lines *)                  
(* Note that in the book extendedlines is the result of the expression: *)
(*   zip extend lines shape *)
(* The zip here is essentially zipWith as it requires a binary function and *)
(* the two lists.  There does not appear to be an equivalent to this in the *)
(* Basis library, and so we use the library zip, and then map extend over *)
(* the result.  This requires us to amend extend to take a pair. *)
fun mkpic lines =
    let val d = length lines
        val shape = map size lines
        val maxw = maxposlist shape
        fun extend (line, len) = if len < maxw
                                 then line ^ spaces (maxw - len)
                                 else line
        val extendedlines = map extend (ListPair.zip (lines, shape))
    in (d, maxw, extendedlines) end
                  
fun depth (d, w, sl) = d
fun width (d, w, sl) = w
fun linesof (d, w, sl) = sl

(* extends the lines of pic if they are shorter than n with an appropriate *)
(* number of spaces. *)
(* Note that the picture is returned unaltered if it is already as wide, or *)
(* wider than n.  This behviour is described on page 129, but the actual *)
(* implementation provided assumed that n >= w. *)
fun padside n (d, w, sl) =
    if n <= w
    then (d, w, sl)
    else (d, n, map (fn (s) => s ^ spaces (n - w)) sl)

(* extends the number of lines in a picture to n using blank lines at the bottom *)
(* Note that the picture is returned unaltered if it is already as deep, or *)
(* deeper than n.  This behaviour is described on page 129, but the actual *)
(* implementation provided assumed that n >= d. *)
fun padbottom n (d, w, sl) =
    if n <= d
    then (d, w, sl)
    else (n, w, sl @ copy (n - d) (spaces w))

(* returns a picture by putting the pictures in piclist side-by-side. *)
(* pictures are separated by a column of s, with f on the left-hand edge *)
(* and b on the right-hand edge. *)
(* Note that in the book mkline uses *)
(*   map (select n) blocks *)
(* where fun select n = hd o (drop (n -1)) *)
(* and fun drop n = repeat n tl *)
(* We use nth from the Basis library here to avoid defining two auxiliary *)
(* functions, but as this has type 'a list * int -> 'a (list as the first *)
(* argument), we need a lambda to partially apply the second argument. *)
(* nth is also zero based, and so we mkline from 0 upto (maxd -1) rather than *)
(* the book's 1 upto maxd. *)
fun rowwith fsb piclist =
    let val maxd = maxposlist (map depth piclist)
    in
        if maxd = 0
        then nullpic
        else let val blocks = map (linesof o padbottom maxd) piclist
                 fun mkline n = stringwith fsb (map (fn (lines) => List.nth(lines, n)) blocks)
                 val sl' = map mkline (0 upto (maxd - 1))
                 val w' = size (hd sl')
             in
                 (maxd, w', sl')
             end
    end

val row = rowwith("", "", "")

(* returns a picture by stacking the pictures in piclist vertically in a column. *)
(* fsb are used as the top, separator and bottom respectively, and if any of *)
(* them consists of more than one character, a line for each character is formed *)
(* ie. the strings are written downwards and duplicated along a row to the width *)
(* of the final picture. *)
(* Note: explicitly typed piclist here to avoid 'Warning: type vars not *)
(* generalized because of value restriction are instantiated to dummy types *)
(* (X1,X2,...)' in relation to picture depth.  As an operation is not applied *)
(* to depth here, it retains the polymorphic type 'a otherwise. *)
fun colwith (f, s, b)(piclist: picture list) =
    let val maxw = maxposlist (map width piclist)
        val flines = map (implode o (copy maxw)) (explode f)
        val slines = map (implode o (copy maxw)) (explode s)
        val blines = map (implode o (copy maxw)) (explode b)
        val sl' = linkwith (flines, slines, blines) (map (linesof o padside maxw) piclist)
        val d' = length sl'
    in
        (d', maxw, sl')
    end

val column = colwith("", "", "")

(* when applied to n >= 0 and a picture pic, forms a picture with n leading *)
(* spaces before each line of pic. *)
fun indent n (d, w, sl) =
    if n < 1
    then (d, w, sl)
    else (d, w + n, map (concat (spaces n)) sl)

(* when applied to n >= 0 and a picture pic, forms a picture with n blank *)
(* lines followed by the lines of pic. *)
fun lower n (d, w, sl) =
    if n < 1
    then (d, w, sl)
    else (d + n, w, copy n (spaces w) @ sl)

(* produces a picture by surrounding the argument picture with a box drawn *)
(* using "-" and "|" and "+" at the corners. *)
fun frame pic =
    let val pic' = rowwith("|", "", "|") [pic]
        val edge = mkpic ["+" ^ dashes (width pic) ^ "+"]
    in
        column [edge, pic', edge]
    end

(* returns a single string (containing newline characters) displaying the *)
(* lines of a picture. *)
(* Note that the result can be printed to the console with *)
(*   print(showpic pic) *)
(* or *)
(*   TextIO.output(TextIO.stdOut, (showpic pic)) *)
fun showpic (d, w, sl) =
    stringwith("","\n","\n") sl

(* makes a list of lists of pictures rectangular, filling in short lists with *)
(* default as appropriate. *)
(* Note that in the book the result expression is: *)
(*   zip extend shape listlist *)
(* The zip here is essentially zipWith as it requires a binary function and *)
(* the two lists.  There does not appear to be an equivalent to this in the *)
(* Basis library, and so we use the library zip, and then map extend over *)
(* the result.  This requires us to amend extend to take a pair. *)      
fun mkrect listlist default =
    let val shape = map length listlist
        val maxrow = maxposlist shape
        fun extend (len, list) =
            if len < maxrow
            then list @ (copy (maxrow - len) default)
            else list
    in
        map extend (ListPair.zip (shape, listlist))
    end
        
(* returns a picture consisting of a table built from the supplied list of rows *)
(* Note that the book generates picrowlists with the expression: *)
(*   map (zip padside colwidths) newpics *)
(* The zip here is essentially zipWith as it requires a binary function and the *)
(* two lists.  There does not appear to be an equivalent to this in the Basis *)
(* library, and so we use the library zip, and then map padside over the result. *)
(* This requires us to use a lambda function to destructure the pair, as padside *)
(* has curried form. *)
fun table [] = nullpic
  | table piclistlist =
    let val newpics = mkrect piclistlist nullpic
        val picwidths = map (map width) newpics
        val colwidths = map maxposlist (transpose picwidths)
        fun widen piclist =
            map (fn (w, pic) => padside w pic) (ListPair.zip(colwidths, piclist))
        val picrowlists = map widen newpics
        val tablerows = map (rowwith("|", "|", "|")) picrowlists
        val sep = stringwith("+", "+", "+")(map dashes colwidths)
        val sl' = linkwith([sep], [sep], [sep])(map linesof tablerows)
        val d' = length sl'
        val w' = size(hd sl')
    in
        (d', w', sl')
    end

(* when overlaying character b onto character a the result is character b *)
fun overlay a b = b

(* places pic2 on top of pic1 at the point after n characters down and m *)
(* characters along. *)
(* If n < 0 then pic2 will start above pic1. *)
(* If m < 0 then pic2 will start to the left of pic1. *)
fun paste n m pic1 pic2 =
    if n < 0 then paste 0 m (lower (~n) pic1) pic2
    else if m < 0 then paste n 0 (indent (~m) pic1) pic2
    else paste0 n m pic1 pic2
                   
(* places pic2 on top of pic1 at the point after n characters down and m *)
(* characters along. *)
(* assumes both n and m are non-negative. *)
and paste0 n m pic1 pic2 =
    let val pic1' = padbottom (n + depth pic2) (padside (m + width pic2) pic1)
        fun stringop line line' =
            implode (spliceat m overlay (explode line) (explode line'))
        val sl' = spliceat n stringop (linesof pic1') (linesof pic2)
        val w' = if null sl'
                 then 0
                 else size (hd sl')
        val d' = length sl'
    in
        (d', w', sl')
    end

(* produces a picture of depth d and width w cut from pic starting at the point *)
(* after n characters down and m characters along. *)
(* If d or w is negative, then the picture is cut back from and/or above the *)
(* (n,m) coordinate.  If m or n is negative, the picture is indented and/or *)
(* lowered before cutting. *)
fun cutfrom pic n m d w =
    if n < 0 then cutfrom (lower (~n) pic) 0 m d w
    else if m < 0 then cutfrom (indent (~m) pic) n 0 d w
    else if d < 0 then cutfrom pic (n + d) m (~d) w
    else if w < 0 then cutfrom pic n (m + w) d (~w)
    else cut0 pic n m d w

(* produces a picture of depth d and width w cut from pic starting at the point *)
(* after n characters down and m characters along. *)
(* assumes n, m, d, w are all non-negative. *)
(* Note that the book uses its own 1-based substring function: *)
(* int -> int -> string -> string.  Instead, we use the Basis library's 0-based *)
(* substring: string * int * int -> string *)         
(* The book also uses a 1-based sublist function, whereas we have implemented *)
(* a 0-based version for consistency. *)        
and cut0 pic n m d w =
    let val pic' = padbottom (n + d) (padside (m + w) pic)
        fun edit str = substring (str, m, w)
        val newsl = map edit (sublist n d (linesof pic'))
    in
        (d, w, newsl)
    end

(* produces a picture with depth d and width w which is made up of copies of *)
(* tile. *)
fun tilepic d w tile =
    let val dt = depth tile
        val wt = width tile
        val ndeep = (d + dt - 1) div dt
        val nacross = (w + wt - 1) div wt
        val col = column(copy ndeep tile)
        val excess = row(copy nacross col) 
    in
        cutfrom excess 0 0 d w
    end

        
(* *************** *)
(* Sample Pictures *)
(* *************** *)
val p0 = frame(mkpic ["this is", "a", "sample picture"])

(* The following pictures are displayed in figure 4.1 on page 123 *)
val p1 = paste 3 5 p0 p0

val p2 = row [p1, p1, frame p1]

(* The third picture is constructed from a tile picture called alpha *)
val alpha = mkpic ["ABCD", "EFGH", "IJKL"]
val p3 = tilepic 8 23 alpha

(* The Fourth picture is a table made from 12 basic pictures arranged in 3 rows *)
val bp1 = mkpic ["Column 1"]
val bp2 = mkpic ["Column 2"]
val bp3 = mkpic ["Column 3"]
val bp4 = mkpic ["Row 1"]
val bp5 = mkpic ["John"]
val bp6 = mkpic ["Mary", "Jane"]
val bp7 = mkpic ["New York"]
val bp8 = mkpic ["Row 2"]
val bp9 = mkpic ["Chris", "Diane"]
val bp10 = mkpic ["Nicola", "Alex"]
val bp11 = mkpic ["Paris", "London", "Edinburgh"]
val p4 = table [[nullpic, bp1, bp2, bp3],
                [bp4, bp5, bp6, bp7],
                [bp8, bp9, bp10, bp11]]
               
val p5 = table [[paste 4 5 p3 (frame p3), p3],
                [alpha, alpha, alpha]]

(* The following pictures are displayed in figure 4.3 on page 136 *)
val pic1 = frame (mkpic (copy 10 "picture 1 picture 1 picture 1 "))
val pic2 = frame (mkpic (copy 3 "picture 2 picture 2 picture 2 picture 2 "))
val pic3 = paste 4 ~8 pic1 pic2
val pic4 = cutfrom pic3 2 5 6 30
