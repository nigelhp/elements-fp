
(* pads a string representation into a window of size w *)
(* a string of asterisks is produced if the window size is not large enough *)
(* a negative window size is an error and the result is undefined in this case *)

exception NegativeArgumentException
              
local
    (* stringcopy *)
    fun stringcopy0(0, s) = ""
      | stringcopy0(n, s) = stringcopy0(n - 1, s) ^ s
    fun stringcopy(n, s) =
        if n < 0 then
            raise NegativeArgumentException
        else
            stringcopy0(n, s)
    fun spaces n =
        stringcopy(n, " ")
    fun asterisks n =
        stringcopy(n, "*")
                  
    (* stringofint & auxiliary functions *)
    fun digittochar n =
        chr(n + ord #"0")
    fun digittostr n =
        str(digittochar n)
    fun stringofnat n =
        if n < 10 then
            digittostr n
        else
            stringofnat(n div 10) ^ digittostr(n mod 10)
    fun stringofint n =
        if n < 0 then
            "~" ^ stringofnat(~n)
        else
            stringofnat(n)
in
(* left justify s in a window of size w, e.g. *)
(* windowL(5, "abc") -> "abc  " *)
(* windowL(3, "abc") -> "abc" *)
(* windowL(2, "abc") -> "**" *)
fun windowL(w, s) =
    let
        val swidth = size s
    in
        if w < swidth then
            asterisks(w)
        else
            s ^ spaces(w - swidth)
    end

(* right justify s in a window of size w, e.g. *)
(* windowR(5, "abc") -> "  abc" *)
(* windowR(3, "abc") -> "abc" *)
(* windowR(2, "abc") -> "**" *)
fun windowR(w, s) =
    let
        val swidth = size s
    in
        if w < swidth then
            asterisks(w)
        else
            spaces(w - swidth) ^ s
    end

(* centrally justifies s in a window of size w, e.g. *)
(* windowC(7, "abc") -> "  abc  " *)
(* windowC(6, "abc") -> " abc  " *)
(* windowC(5, "abc") -> " abc " *)
(* windowC(4, "abc") -> "abc " *)
(* windowC(3, "abc") -> "abc" *)
(* windowC(2, "abc") -> "**" *)        
fun windowC(w, s) =
    let
        val swidth = size s
        val lpadw = Int.max(((w - swidth) div 2), 0)
        val rpadw = w - swidth - lpadw
    in
        if w < swidth then
            asterisks(w)
        else
            spaces(lpadw) ^ s ^ spaces(rpadw)
    end

(* right-justify the string representation of the integer n in a window of size w, e.g. *)
(* windowint(4, 42) -> "  42" *)
(* windowint(2, 42) -> "42" *)
(* windowint(1, 42) -> "*" *)        
fun windowint(w, n) =
    windowR(w, stringofint(n))
end
