(* A traditional solution is to keep the associations as a list and to search *)
(* down the list for the most recent association for a given value. *)

(* Since associations are used to lookup associated values, we identify the *)
(* association with the lookup function itself. *)
type ('k, 'v) associations = 'k -> 'v

(* assoc pairlist oldassociations is a function 'k -> 'v, which when given a *)
(* value to lookup, will return the associated value in pairlist (or *)
(* oldassociations). *)
(* Note that this function is curried to allow partial application of pairlist *)
(* and oldassocs. *)
fun assoc [] oldassocs k =
    oldassocs k 
  | assoc ((k1, v1) :: rest) oldassocs k =
    if k1 = k then v1
    else assoc rest oldassocs k

(* We do not need to define lookup, which degenerates to the identity function. *)
(* lookup assocs k = assocs k *)

(* examples *)
exception NoSuchAssociation
                                   
fun emptyassoc k =
    raise NoSuchAssociation

val example1 = assoc [(1, "a"), (2, "b"), (3, "c")] emptyassoc
val example2 = assoc [(2, "e"), (4, "xxx")] example1;

example1 1 = "a";
example1 2 = "b";
example1 3 = "c";
(example1 4 handle NoSuchAssociation => "") = "";

example2 1 = "a";
example2 2 = "e";
example2 3 = "c";
example2 4 = "xxx";
(example2 5 handle NoSuchAssociation => "") = "";

fun identity x =
    x

val atlocation = assoc [(1, 100), (6, 50), (7, 200)] identity;

atlocation 1 = 100;
atlocation 2 = 2;
atlocation 3 = 3;
atlocation 4 = 4;
atlocation 5 = 5;
atlocation 6 = 50;
atlocation 7 = 200;
