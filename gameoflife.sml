(* 4.2 Game of Life *)

type generation = (int * int) list

local
    (* sort coordinates removing duplicates *)
    fun lexordset [] = [] 
      | lexordset (a :: x) = lexordset (List.filter (lexless a) x) @ [a] @
                             lexordset (List.filter (lexgreater a) x)
    and lexless (a1, b1) (a2, b2) =
        if a2 < a1 then true else
        if a2 = a1 then b2 < b1 else false
    and lexgreater pr1 pr2 = lexless pr2 pr1

    fun member xs x =
        List.exists (fn a => a = x) xs

    (* finds coords which occur exactly 3 times in coords *)
    fun occurs3 coords =
        let fun f xover x3 x2 x1 [] = diff x3 xover 
              | f xover x3 x2 x1 (a :: x) =
                if member xover a then f xover x3 x2 x1 x else
                if member x3 a then f (a :: xover) x3 x2 x1 x else
                if member x2 a then f xover (a :: x3) x2 x1 x else
                if member x1 a then f xover x3 (a :: x2) x1 x else
                f xover x3 x2 (a :: x1) x
            and diff x y =
                List.filter (not o member y) x
        in
            f [] [] [] [] coords
        end
            
    fun twoorthree n =
        n = 2 orelse n = 3

    (* reverse y onto x *)
    (* e.g. revonto [x1,x2,...,xn][y1,y2,...,ym] = [ym,ym-1,...,y1,x1,x2,...,xn] *)
    fun revonto x y =
        List.foldl (fn (y',xs) => y' :: xs) x y
                  
    fun collect f list =
        let fun accumf sofar [] = sofar 
              | accumf sofar (a :: x) = accumf (revonto sofar (f a)) x
        in
            accumf [] list
        end
in
(* a kind of inverse for mkgen - the identity function *)
fun alive livecoords = livecoords

(* takes an arbitrary list of coordinates and removes repetitions whilst *)
(* sorting them *)                           
fun mkgen coordlist = lexordset coordlist

(* Rules for calculating the next generation: *)
(* 1.  The next generation is the union of the survivors and the newborn, *)
(*     calculated simultaneously from the live cells of the current generation. *)
(* 2.  A live cell is a survivor in the next generation if it has either 2 or 3 *)
(*     live neighbours in the current generation.  (It dies from loneliness or  *)
(*     overcrowding otherwise.) *)
(* 3.  A dead cell becomes a newborn, live cell in the next generation if it *)
(*     has exactly three live neighbours in the current generation. *)
(* Note that neighbours has type (int * int) -> (int * int) list.  This allows *)
(* us to experiment with different sorts of board. *)
fun mk_nextgen_fn neighbours gen =
    let val living = alive gen
        val isalive = member living
        val liveneighbours = length o List.filter isalive o neighbours
        val survivors = List.filter (twoorthree o liveneighbours) living
        val newnbrlist = collect (List.filter (not o isalive) o neighbours) living
        val newborn = occurs3 newnbrlist
    in
        mkgen (survivors @ newborn)
    end
end

exception NegativeArgumentException
              
local
    fun repeat f =
        let fun rptf n x =
                if n = 0 then x
                else rptf (n - 1) (f x)
            fun check n =
                if n < 0 then raise NegativeArgumentException else n
        in
            rptf o check
        end
               
    fun cons a x =
        a :: x
                 
    fun copy n x =
        repeat (cons x) n []
               
    fun spaces n =
        implode (copy n #" ")

    val xstart = 0 and ystart = 0

    fun markafter n str =
        str ^ spaces n ^ "0"
                             
    (* (x, y)             = current position *)
    (* str                = current line being prepared *)
    (* ((x1, y1) :: more) = coordinates to be plotted *)
    fun plotfrom (x, y) str ((x1, y1) :: more) =
        if x = x1
        then (* same line so extend str and continue from y1 +1 *)
            plotfrom (x, y1 + 1) (markafter (y1 - y) str) more
        else (* flush current line and start a new line *)
            str :: plotfrom (x + 1, ystart) "" ((x1, y1) :: more) 
      | plotfrom (x, y) str [] = [str] 
in
fun plot coordlist = plotfrom (xstart, ystart) "" coordlist
end

val glider = [(0, 0), (0, 2), (1, 1), (1, 2), (2, 1)]
val bail = [(0,0), (0, 1), (1, 0), (1, 1)]
val blinker = [(0, 0), (1, 0), (2, 0)]
val spaceship = [(0, 4), (1, 5), (2, 0), (2, 5), (3, 1), (3, 2), (3, 3), (3, 4)]
                    
infix 6 at
fun coordlist at (x, y) =
    let fun move (a, b) =
            (a + x, b + y)
    in
        map move coordlist
    end

(* assuming infinite reguar board *)        
fun neighbours (i, j) =
    [(i-1, j-1), (i-1, j), (i-1, j+1),
     (i, j-1), (i, j+1),
     (i+1, j-1), (i+1, j), (i+1, j+1)]
        
(* main - some simple functions to let us see something ...*)
        
(* returns the elements of list separated by sep, *)
(* with a leading front and trailing back *)
fun stringwith (front, sep, back) list =
    let fun sepback [] = back 
          | sepback [h] = h ^ back
          | sepback (h :: t) = h ^ sep ^ sepback t
    in
        front ^ sepback list
    end

(* returns a single string (containing newline characters) displaying the *)
(* lines of a picture. *)
(* Note that the result can be printed to the console with *)
(*   print(showpic pic) *)
(* or *)
(*   TextIO.output(TextIO.stdOut, (showpic pic)) *)
fun showpic lines =
    stringwith("**********","\n","\n") lines

fun display gen =
    print (showpic (plot (alive gen)))
          
fun main gen 0 =
    display gen
  | main gen n =
    (display gen;
     main (mk_nextgen_fn neighbours gen) (n - 1))

        
val startgen = mkgen (glider at (3, 4));
main startgen 12;
