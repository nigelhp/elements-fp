(* from Bird / Wadler, "Introduction to Functional Programming" - Section 4.1 *)

(* Notes: *)
(* - to handle list indices commencing at zero, the authors use a pattern match *)
(*   to reduce the list index, such as: *)
(*     afunction (u + 1) = ... somelist[u] ... *)
(*   Such a pattern is not supported by SML, and for simplicity we pad such *)
(*   lists with dummy initial elements. *)

val units = ["", "one", "two", "three", "four", "five",
             "six", "seven", "eight", "nine"]
val teens = ["ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen",
             "sixteen", "seventeen", "eighteen", "nineteen"]
val tens = ["", "", "twenty", "thirty", "forty", "fifty", "sixty",
            "seventy", "eighty", "ninety"]
                
fun digits2 n =
    (n div 10, n mod 10)

(* returns a pair (h,t) where: *)
(*   h is the hundreds part of n (0 <= h < 10) *)
(*   t is the part < 100 *)        
fun digits3 n =
    (n div 100, n mod 100)

fun digits6 n =
    (n div 1000, n mod 1000)
        
fun combine2 (0, u) =
    List.nth (units, u)
  | combine2 (1, u) =
    List.nth (teens, u) 
  | combine2 (t, 0) =
    List.nth (tens, t)
  | combine2 (t, u) =
    List.nth (tens, t) ^ "-" ^ List.nth (units, u)

fun convert2 n =
    combine2 (digits2 n)
                                                      
fun combine3 (0, t) =
    convert2 t
  | combine3 (h, 0) =
    List.nth (units, h) ^ " hundred"
  | combine3 (h, t) =
    List.nth (units, h) ^ " hundred and " ^ convert2 t
                                                     
fun convert3 n =
    combine3 (digits3 n)

fun link h =
    if h < 100
    then " and "
    else " "
             
fun combine6 (0, h) =
    convert3 h
  | combine6 (m, 0) =
    convert3 m ^ " thousand"
  | combine6 (m, h) =
    convert3 m ^ " thousand" ^ link h ^ convert3 h
    
(* 0 < n < 1000000 *)
fun convert n =
    combine6 (digits6 n)


(* convert 308000 -> three hundred and eight thousand*)
(* convert 369027 -> three hundred and sixty-nine thousand and twenty-seven *)
(* convert 369401 -> three hundred and sixty-nine thoustand four hundred and one *)
