
local
    fun binrepnat 0 = "0"
      | binrepnat 1 = "1"
      | binrepnat n = binrepnat(n div 2) ^ binrepnat(n mod 2)
in
(* naive implementation that does not express negative numbers in 2s complement *)
fun binrepint n =
    if n < 0 then
        "~" ^ binrepnat(~n)
    else
        binrepnat n
end
